"""
Created on Sat Oct 26 22:48:21 2019         "Multi_LSTM_TrainTest.py"
@author: hito             during a vacation trip to Shogen-ji, Japan.
"""

##  - Prediction of Event occurrence (time of event, event value). Extendable to multi-dimensioanal event vector-                                                    Nov21, 2019
##
##  This program grabs the data from .csv file, cuts to bunches of short-stepped data for training, trains the multivariate LSTM in Tensorflow.
##  Training data has the structure:  'n_steps(3)' sets of {time, event value} and a set teacher's solution {time, event value}.
##  These data are extracted from .csv file, 80% for training and 20% for testing. Inference test results are given in graphic display, where 
##  the red blob is the predition and the 4th blue dot is the ground truth.


# multivariate output stacked lstm example
import numpy as np
from keras.layers import LSTM
from keras.layers import Dense
import csv
from numpy import array
from numpy import hstack
from keras.models import Sequential
import time

import matplotlib.pyplot as plt
debug = 0

# input data structure [0]numper of pairs [1]time of eveny [2]value [3]time of event [4]value .....[2xnumber of pairs]
data_file='model1.csv'
EPOCHS=50

# split a multivariate sequence into samples
def split_sequences(sequences, n_steps):
    
    X, y = list(), list()
    for i in range(len(sequences)):
		# find the end of this pattern
        end_ix = i + n_steps
		# check if we are beyond the dataset
        if end_ix > len(sequences)-1:
            break
		# gather input and output parts of the pattern
        seq_x, seq_y = sequences[i:end_ix, :], sequences[end_ix, :]
        X.append(seq_x)
        y.append(seq_y)
    return array(X), array(y)

# split a multivariate sequence into samples: The first set to be set as (0,0)
def split_sequences_norm(sequences, n_steps):
	X, y = list(), list()
    #Xn, yn =list(), list()
	for i in range(len(sequences)):  #11
		# find the end of this pattern
		end_ix = i + n_steps
        #top_ix = i
		# check if we are beyond the dataset
		if end_ix > len(sequences)-1:
			break
		# gather input and output parts of the pattern
		seq_x, seq_y = sequences[i:end_ix, :]-sequences[i, :], sequences[end_ix, :] - sequences[i,:]
		X.append(seq_x)
		y.append(seq_y)                        
	return array(X), array(y)

################################   main  ########################################################################
#### Preparing the full train- and test-datasets {Xfull_train, Yfull_train} and {Xfull_train, Yfull_train}#######
        
with open(data_file, 'r', newline='') as csvfile:
    reader= csv.reader(csvfile, delimiter=',')   
    line_number = 0
    data_count = 0
    test_count = 0
    train_count = 0
      
    Xfull_train = list()
    Yfull_train = list()
    Xfull_test = list()
    Yfull_test = list()
    
    for row in reader:
        trainset=[]
        testset=[]
        row_norm=[]
        col_norm=[]
        row_test=[]
        col_test=[]
        num_pair= int(row[0]);
        
        if num_pair >3:       
            if data_count%10 != 0: ### Training data ~90%
                for i in range(num_pair):
                    rowCenter= float(row[2*i+1])
                    colCenter= float(row[2*i+2])
                    row_norm.append(rowCenter)
                    col_norm.append(colCenter)
              
                in_seq1 = array(row_norm)
                in_seq2 = array(col_norm)  
                in_seq1 = in_seq1.reshape((len(in_seq1), 1))
                in_seq2 = in_seq2.reshape((len(in_seq2), 1))
                trainset = hstack((in_seq1, in_seq2))
                train_count +=1
     
            else:                  ### Test data ~10%
                for i in range(num_pair):
                    rowCenter= float(row[2*i+1])
                    colCenter= float(row[2*i+2])
                    row_test.append(rowCenter)
                    col_test.append(colCenter)
                
                in_seq3 = array(row_test)
                in_seq4 = array(col_test)
                in_seq3 = in_seq3.reshape((len(in_seq3), 1))
                in_seq4 = in_seq4.reshape((len(in_seq4), 1))
                testset = hstack((in_seq3, in_seq4))
                test_count +=1
         
            data_count +=1     
        line_number +=1
            
# choose a number of the steps for pre_conditional/train9ing in put
# prediction:  n_steps +1        
        n_steps = 3

# convert into input/output
#X, y = split_sequences(dataset, n_steps)
# convert into input/output all relative to (first,first)
        X, y = split_sequences_norm(trainset, n_steps)          
        for k in range(len(X)):
            Xfull_train.append(X[k,:,:])
            Yfull_train.append(y[k,:])
        
# do the same for test data. not relative
        X, y = split_sequences(testset, n_steps)         
        for k in range(len(X)):
            Xfull_test.append(X[k,:,:])
            Yfull_test.append(y[k,:])    

print("train_count")
print(train_count)
print("test_count")
print(test_count)
print("line_number")
print(line_number)


#print("@@@@@ train data @@@@")
Xfull_train=array(Xfull_train)
Yfull_train=array(Yfull_train)

#print("@@@@@ test data @@@@")    
Xfull_test=array(Xfull_test)
Yfull_test=array(Yfull_test)
#print(Xfull_test[:10])  
#print(Yfull_test[:10])
#### Completeion of making the full data sets

## number of features to predict = 2, i.e {time,value value}
n_features = 2 ###X.shape[2]



################# setting up LSTM model #######
# define model
model = Sequential()
model.add(LSTM(200, activation='relu', return_sequences=True, input_shape=(n_steps, n_features)))
model.add(LSTM(200, activation='relu'))
model.add(Dense(n_features))
model.compile(optimizer='adam', loss='mse')

################# train the LSTM model #######
model.fit(Xfull_train, Yfull_train, epochs=EPOCHS, verbose=0)


################# testing the LSTM prediction #######
# preparing th etest data
#print("array(Xfull_test).shape")
#print(array(Xfull_test).shape)
aa=array(Xfull_test)
bb=array(Yfull_test)
dim=array(Xfull_test).shape
#print(dim[0]) # data  length

test_count=0
for i in range(dim[0]):
    figure = plt.figure(figsize=(10,6))
    row0=aa[i,0,0]
    col0=aa[i,0,1]
#    print(row0)
#    print(col0)

    x_input = array([[aa[i,0,0]-row0, aa[i,0,1]-col0], [aa[i,1,0]-row0, aa[i,1,1] -col0], [aa[i,2,0]-row0, aa[i,2,1] -col0]] )
#   print(x_input.shape)
    x_input = x_input.reshape((1, n_steps, n_features))

# predict
    yhat = model.predict(x_input, verbose=0)

# show data    
    print("\ntest_count ", test_count)
    print("                time       value")
    print("                %.3f   %.3f " % (aa[i,0,0], aa[i,0,1]))
    print("                %.3f   %.3f " % (aa[i,1,0], aa[i,1,1]))
    print("                %.3f   %.3f " % (aa[i,2,0], aa[i,2,1]))
    #print(yhat[0,0]+row0, yhat[0,1]+col0)
    print("Ground Truth    %.3f   %.3f " % (  bb[i,0], bb[i,1]   ))  
    print("LSTM prediction %.3f   %.3f " % (yhat[0,0]+row0, yhat[0,1]+col0))
    t=np.zeros(4)
    f=np.zeros(4)
    
    t[0]=aa[i,0,0]
    t[1]=aa[i,1,0]
    t[2]=aa[i,2,0]
    t[3]=bb[i,0]
    T   = yhat[0,0]+row0
    devT=abs(T-t[3])
    print("devT rate %f  %f" %(devT, devT/t[3]))
    
    f[0]=aa[i,0,1]
    f[1]=aa[i,1,1]
    f[2]=aa[i,2,1]
    f[3]=bb[i,1]
    F   = yhat[0,1]+col0
    devF=abs(F-f[3])
    print("devF rate %f  %f" %(devF, devF/f[3]))

# plot    
    plt.plot([f[0],f[1],f[2],f[3]], [t[0],t[1],t[2],t[3]],'-bo', markersize=10)
    plt.plot([F],[T], 'ro', markersize=20, alpha=0.2)
    plt.xlim(0.0, 255.0)
    plt.ylim(0.0, 255.0)
    plt.title(data_file)
    plt.ylabel('Time')
    plt.xlabel('Value')
    plt.show()
    time.sleep(1)   
    test_count +=1


